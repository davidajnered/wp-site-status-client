
# WP Site Status Client
Contributors: davidajnered
Donate link: https://wpsitestatus.io
Tags: management dashboard, site status, plugin status, plugin update manager, manage multiple wordpress sites
Requires at least: 4.0.0
Tested up to: 5.2.3
Requires PHP: 7.0
Stable tag: 5.2.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

WP Site Status gives you an overview of all you sites and make it easy for you to keep your sites and plugins up to date.

# Description
WP Site Status gives you an overview of all you sites and make it easy for you to keep your sites and plugins up to date.

Installing the plugin and add your client token for your account on [wpsitestatus.io](https://wpsitestatus.io  "WP Site Status") to enable your site and send information about your site and plugins to WP Site Status.

# Installation
1. Upload `wp-site-status-client` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Add the client token on the WP Site Status Client settings page
4. The plugin will send initial once the token is saved. You can update the information from the settings page as well as from [wpsitestatus.io](https://wpsitestatus.io  "WP Site Status")

# Frequently Asked Questions

## I need help!
Feel free to contact me at support@wpsitestatus.io if you have questions or feedback.

# Screenshots
1. Screenshot of the interface on https://wpsitestatus.io.

# Changelog

### 1.0
* First version of the plugin.

### 1.1
* Rewrite of plugin to make it more modular for futher development.

## Upgrade Notice

### 1.0
First version of the plugin.

### 1.1
You have to deactivate and reactivate plugin to make it work again.
