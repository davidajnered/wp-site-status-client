<?php

namespace App;

class DataTransporter
{
    /**
     * @var boolean
     */
    private $devMode = false;

    /**
     * Constructor.
     *
     * @param boolean $devMode
     */
    public function __construct()
    {
        $this->devMode = (bool) get_site_option('wp_site_status_client_dev_mode');
    }

    /**
     * Send data to server.
     *
     * @var array $data
     * @var string $token
     */
    public function send($data, $token)
    {
        $payload = [
            'body' => [
                'token' => $token,
                'sites' => $data,
            ],
            'sslverify' => false,
            'timeout' => 30,
        ];

        if ($this->devMode === true) {
            $response = wp_remote_post('https://dev.wpsitestatus.io/api/client', $payload);
        } else {
            $response = wp_remote_post('https://wpsitestatus.io/api/client', $payload);
        }

        if (is_wp_error($response)) {
            return [
                'code' => 400,
                'messsage' => $response->get_error_message()
            ];
        } else if (wp_remote_retrieve_response_code($response) !== 200) {
            return [
                'code' => $response['response']['code'],
                'message' => $response['body']
            ];
        } else {
            return [
                'code' => 200,
                'message' => 'Success'
            ];
        }
    }
}