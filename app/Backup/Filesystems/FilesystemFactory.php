<?php

namespace App\Backup\Filesystems;

use App\Backup\Config\Config;

class FilesystemFactory
{
    /**
     * Create a filesystem from config.
     *
     * @param Config $config
     * @return Filesystem
     */
    public static function create(Config $config)
    {
        $type = $config->get('type');
        if ($type === 'local') {
            return new LocalFilesystem($config);
        } else if ($type === 'dropbox') {
            return new DropboxFilesystem($config);
        }
    }
}