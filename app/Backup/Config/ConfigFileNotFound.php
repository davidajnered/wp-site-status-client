<?php

namespace App\Backup\Config;

/**
 * Class ConfigFileNotFound.
 */
class ConfigFileNotFound extends \Exception
{
}
