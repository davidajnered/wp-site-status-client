<?php

namespace App\Backup\Config;

class FilesystemConfig extends Config
{
    /**
     * Load config from settings specific to filesystem.
     *
     * @todo: Implement support for Google Drive, AWS, SFTP and FTP.
     *
     * @return Config
     */
    public static function fromSetting()
    {
        $config = [];
        $wpssc_filesystem = get_site_option('wp_site_status_client_filesystem');

        if ($wpssc_filesystem === 'local') {
            $config = [
                'DB_NAME' => DB_NAME,
                'DB_USER' => DB_USER,
                'DB_PASSWORD' => DB_PASSWORD,
                'DB_HOST' => DB_HOST,
                'DB_CHARSET' => DB_CHARSET,
                'DB_COLLATE' => DB_COLLATE,
            ];
        } else if ($wpssc_filesystem === 'dropbox') {
            $config = get_site_option('wp_site_status_client_filesystem_settings');
            $config['callback_url'] = get_site_url(null, '/wp-json/wp-site-status-client/dropbox/auth/callback');
        }

        $config['type'] = $wpssc_filesystem;

        return new self($config);
    }
}