<?php

namespace App\Backup\Config;

/**
 * Class ConfigFieldNotFound.
 */
class ConfigFieldNotFound extends \Exception
{
}
