<?php

namespace App\Backup\Databases;

use App\Backup\Config\DatabaseConfig;

abstract class Database
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * Constructor.
     *
     * @param DatabaseConfig $config
     */
    public function __construct(DatabaseConfig $config)
    {
        $this->config = $config;
    }

    /**
     * Generate name for backup file.
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->config->get('DB_NAME') . '-' . date('Ymd-His') . '.sql';
    }

    /**
     * Dump database.
     *
     * @return string filepath.
     */
    abstract public function dump();

    /**
     * Restore database. Not yet implemented
     *
     * @param string $filePath
     * @return bool
     */
    abstract public function restore($filePath);
}