<?php

namespace App\Backup;

use App\Backup\Databases\Database;
use App\Backup\Filesystems\Filesystem;

class BackupProcedure
{
    /**
     * @var Database
     */
    private $database;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * Constructor.
     *
     * @param Database $database
     * @param Filesystem $filesystem
     */
    public function __construct(Database $database, Filesystem $filesystem)
    {
        $this->database = $database;
        $this->filesystem = $filesystem;
    }

    /**
     * Run procedure.
     *
     * @todo: add try/catch and return something useful.
     *
     * @return void
     */
    public function run()
    {
        // Delete any old temp files
        $this->filesystem->deleteTempFiles(WPSSC_TEMP_UPLOAD_PATH);

        // Create dump
        $filePath = $this->database->dump();

        // Upload to filesystem
        $response = $this->filesystem->move($filePath);

        // Remove dump
        $this->filesystem->delete($filePath);

        return [
            'code' => $response['code'],
            'message' => $response['message']
        ];
    }
}