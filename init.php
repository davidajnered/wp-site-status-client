<?php
/**
 * Plugin Name: WP Site Status Client
 * Description: Collects and pushes information about your site to your account on wpsitestatus.io.
 * Author: David Ajnered
 * Version: 1.1.1
 */

require __DIR__ . '/vendor/autoload.php';

define('WPSSC_ROOT_FILE', __FILE__);
define('WPSSC_PATH', plugin_dir_path(__FILE__));
define('WPSSC_UPLOAD_PATH', ABSPATH . 'wp-content/uploads/wp-site-status/');
define('WPSSC_TEMP_UPLOAD_PATH', ABSPATH . 'wp-content/uploads/wp-site-status/temp/');
define('WPSSC_BACKUP_UPLOAD_PATH', ABSPATH . 'wp-content/uploads/wp-site-status/backups/');

$url =  'http://' . $_SERVER['SERVER_NAME'];
$tld = end(explode('.', parse_url($url, PHP_URL_HOST)));

define('WPSSC_DEV_MODE', $tld === 'test'); // Automatically set dev mode when on .test domains

new \App\WPSiteStatusClient();
