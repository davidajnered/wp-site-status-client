<div class="wrap">
  <h1>WP Site Status Client</h1>
  <div id="poststuff">
    <form method="POST" action="<?php echo is_multisite() ? '/wp-admin/network/edit.php?action=wp_site_status_client' : 'options.php'; ?>">
      <?php settings_fields('wp_site_status_client'); ?>
      <?php do_settings_sections('wp_site_status_client'); ?>

      <!-- Dev start -->
      <?php if (WPSSC_DEV_MODE): ?>
        <div class="postbox">
          <h2 class="hndle"><span>Dev mode</span></h2>
          <div class="inside">
            <label for="wp_site_status_client_dev_mode">
              <input id="wp_site_status_client_dev_mode" type="checkbox" name="wp_site_status_client_dev_mode" value="enabled"<?php echo $wpssc_dev_mode ? ' checked' : ''; ?>/>
              <b>Enable dev mode</b>
            </label>
            <?php submit_button(); ?>
          </div>
        </div>
      <?php endif; ?>
      <!-- Dev end -->

      <!-- Token start -->
      <div class="postbox">
        <h2 class="hndle"><span>Client token</span></h2>
        <div class="inside">
          <p style="max-width: 550px;">WP Site Status Client collects information about your site, plugins and server and sends it to your account on <a href="https://wpsitestatus.io">https://wpsitestatus.io</a>. No personal data is collected, only data about you sites health. For more information visit the <a href="https://wpsitestatus.io/terms#privacy">privacy page</a> on <a href="https://wpsitestatus.io">https://wpsitestatus.io</a>.</p>
          <p style="max-width: 550px;">WP Site Status Client needs a client token to identify your account on <a href="https://wpsitestatus.io">https://wpsitestatus.io</a>. You'll find it on your <a href="https://wpsitestatus.io/account">account page</a>.</p>
          <label for="wp_site_status_client_token"><b>Client token</b></label><br>
          <input id="wp_site_status_client_token" class="regular_text" type="text" name="wp_site_status_client_token" value="<?php echo esc_attr($wpssc_token); ?>" />
          <?php submit_button(); ?>
        </div>
      </div>
      <!-- Token end -->

      <!-- Backup settings start -->
      <div class="postbox">
        <h2 class="hndle"><span>Backup</span></h2>
        <div class="inside">
          <select name="wp_site_status_client_filesystem" id="filesystem">
            <option value=""<?php echo !$wpssc_filesystem ? ' selected' : ''; ?>>No backups</option>
            <!-- <option value="local"<?php echo $wpssc_filesystem === 'local' ? ' selected' : ''; ?>>Local</option> -->
            <option value="dropbox"<?php echo $wpssc_filesystem === 'dropbox' ? ' selected' : ''; ?>>Dropbox</option>
          </select>

          <div class="filesystem-section hidden" id="filesystem-section-local">
            <p>Backup files will be stored in <b>/wp-content/uploads/wp-site-status/backups</b>.</p>
          </div>

          <!-- Dropbox -->
          <div class="filesystem-section hidden" id="filesystem-section-dropbox">
            <p style="max-width: 550px;">
              To use Dropbox as your backup storage you need to <a href="https://www.dropbox.com/developers/apps" target="_blank">create an Dropbox app</a> and connect the plugin to it.
              Click the link, create an app, generate an access token and copy/paste the token in the fields below.
            </p>
            <p style="max-width: 550px;"><b>WP Site Status doesn't save any information about your app anywhere else than on your site.</b></p>
            <div>
              <label for="wp_site_status_client_filesystem_dropbox_token"><b>Access token</b></label><br>
              <input id="wp_site_status_client_filesystem_dropbox_token" class="regular_text" type="text" name="wp_site_status_client_filesystem_settings[access_token]" value="<?php echo esc_attr($wpssc_filesystem_settings['access_token']); ?>" />
            </div>

            <?php if ($wpssc_filesystem_settings['access_token']): ?>
              <p>Your backups are now uploaded to your Dropbox app. <a href="/wp-json/wp-site-status-client/dropbox/revoke">Revoke access</a></p>
            <?php endif; ?>
          </div>

          <?php submit_button(); ?>
        </div>
      </div>
      <!-- Backup settings end -->
    </form>
  </div>
</div>

<script>
  jQuery(function($) {
    $('#filesystem').on('change', function(event) {
      $('.filesystem-section').hide();
      var selector = '#filesystem-section-' + event.target.value;
      if ($(selector)) {
        $(selector).show();
      }
    });

    // Initial
    $('#filesystem-section-' + $('#filesystem').find(":selected").val()).show();
  });
</script>